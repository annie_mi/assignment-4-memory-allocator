#define _DEFAULT_SOURCE
#define INIT_HEAP_SIZE 1


#include <stddef.h>
#include <string.h>

#include <stdio.h>

#include "mem.h"
#include "mem_internals.h"
#include "tests.h"
#include "util.h"

void* map_pages(void const* address, int64_t length) {
    return mmap( (void*) address, length, PROT_READ | PROT_WRITE, MAP_PRIVATE, -1, 0 );
}

static struct block_header * prepare_data_block(void* data) {
    return (struct block_header *) ((uint8_t *) data - offsetof(struct block_header, contents));
}

void free_one_mem_alloc(void* mem) {
    _free(mem);
}

void free_two_mem_alloc(void* mem1, void* mem2) {
    _free(mem1);
    _free(mem2);
}

void free_three_mem_alloc(void* mem1, void* mem2, void* mem3) {
    _free(mem1);
    _free(mem2);
    _free(mem3);
}

void free_four_mem_alloc(void* mem1, void* mem2, void* mem3, void* mem4) {
    _free(mem1);
    _free(mem2);
    _free(mem3);
    _free(mem4);
}

static struct block_header* initialize_heap() {
    
    struct block_header* heap = (struct block_header*) heap_init(INIT_HEAP_SIZE);
    
    if (heap) {
        printf("heap initiated:\n");
        debug_heap(stdout, heap);
        return heap;
    }
    printf("failed to initiate heap");
    return NULL;
}

void test_success_alloc() {
    printf("Test 1: successful allocation\n");
    struct block_header* heap = initialize_heap();
    if (heap == NULL) return;

    void* mem_alloc = _malloc(INIT_HEAP_SIZE / 10);
    if (!mem_alloc || heap->is_free) {
        printf("failed to malloc");
        return;
    }
    printf("heap after malloc:\n ");
    debug_heap(stdout, heap);
    printf("test1 passed!\n");
    free_one_mem_alloc(mem_alloc);
}

void test_free_one_block() {
    printf("Test 2: free one of many blocks\n");
    struct block_header* heap = initialize_heap();
    if (heap == NULL) return;

    void* mem_alloc1 = _malloc(INIT_HEAP_SIZE / 10);
    void* mem_alloc2 = _malloc(INIT_HEAP_SIZE / 10 * 2);
    void* mem_alloc3 = _malloc(INIT_HEAP_SIZE / 10 * 3);

    if (mem_alloc1 && mem_alloc2 && mem_alloc3) {
        debug_heap(stdout, heap);
        free_one_mem_alloc(mem_alloc2);
    }
    else {
        printf("failed while trying to alloc blocks");
        free_three_mem_alloc(mem_alloc1, mem_alloc2, mem_alloc3);
        return;
    }

    if (!mem_alloc1 || !mem_alloc3) {
        printf("Memory damaged!");
        free_two_mem_alloc(mem_alloc1, mem_alloc3);
        return;
    }

    debug_heap(stdout, heap);
    free_two_mem_alloc(mem_alloc1, mem_alloc3);
    printf("test 2 passed\n");
}

void test_free_sev_blocks() {
    printf("Test 3: free several of many blocks\n");
    struct block_header* heap = initialize_heap();
    if (heap == NULL) return;


    void* mem_alloc1 = _malloc(INIT_HEAP_SIZE / 10);
    void* mem_alloc2 = _malloc(INIT_HEAP_SIZE / 10 * 2);
    void* mem_alloc3 = _malloc(INIT_HEAP_SIZE / 10 * 3);
    void* mem_alloc4 = _malloc(INIT_HEAP_SIZE / 10);

    if (mem_alloc1 && mem_alloc2 && mem_alloc3 && mem_alloc4) {
        debug_heap(stdout, heap);
        free_two_mem_alloc(mem_alloc1, mem_alloc2);
    }
    else {
        printf("failed while trying to alloc blocks");
        free_four_mem_alloc(mem_alloc1, mem_alloc2, mem_alloc3, mem_alloc4);
        return;
    }

    if (!mem_alloc3 || !mem_alloc4) {
        printf("Memory damaged!");
        free_two_mem_alloc(mem_alloc3, mem_alloc4);
        return;
    }

    debug_heap(stdout, heap);
    printf("test 3 passed\n");
    free_four_mem_alloc(mem_alloc1, mem_alloc2, mem_alloc3, mem_alloc4);
}

void test_no_mem_in_heap() {
    printf("Test 4: trying to allocate block but there is no memory left in heap\n");
    struct block_header* heap = initialize_heap();
    if (heap == NULL) return;


    void* mem_alloc1 = _malloc(INIT_HEAP_SIZE);

    if (mem_alloc1) {
        debug_heap(stdout, heap);
        void* mem_alloc2 = _malloc(INIT_HEAP_SIZE / 20);
        if (!mem_alloc2) {
            printf("failed while trying to alloc blocks");
            return;
        }
        free_one_mem_alloc(mem_alloc2);
    }
    else {
        printf("failed while trying to alloc blocks");
        return;
    }

    if ( size_from_capacity( heap->capacity ).bytes < 2 * INIT_HEAP_SIZE ) {
        printf("failed to extend the heap");
    }


    debug_heap(stdout, heap);
    printf("test 4 passed\n");
    free_one_mem_alloc(mem_alloc1);
}


void test_no_mem_heap_expends() {
    printf("Test 5: trying to allocate block but there is no memory left in heap. Heap expends in other place\n");
    struct block_header* heap = initialize_heap();
    if (heap == NULL) return;


    void *mem_alloc1 = _malloc(INIT_HEAP_SIZE/2);


    if (mem_alloc1) {
        debug_heap(stdout, heap);
        struct block_header *new_block = heap;
        while (new_block->next) {
            new_block = new_block->next;
        }
        map_pages((uint8_t *) new_block + size_from_capacity(new_block->capacity).bytes, 1000);

        void *mem_alloc2 = _malloc(INIT_HEAP_SIZE * 3);
        debug_heap(stderr, heap);

        struct block_header *prepared_data2_block = prepare_data_block(mem_alloc1);

        if (!mem_alloc2) {
            printf("failed to allocate block");
            free_one_mem_alloc(mem_alloc1);
            return;
        }
        else if (prepared_data2_block == new_block) {
            printf("first block is near first heap!\n");
            free_two_mem_alloc(mem_alloc1, mem_alloc2);
            return;
        }

        printf("test passed\n");
        free_two_mem_alloc(mem_alloc1, mem_alloc2);
    }
    else {
        printf("failed to allocate block");
    }
}

void run_tests() {
    test_success_alloc();
    test_free_one_block();
    test_free_sev_blocks();
    test_no_mem_in_heap();
    test_no_mem_heap_expends();
}
